class CreateTables < ActiveRecord::Migration[5.2]
  def change
    create_table :tables do |t|

      t.string :nama
      t.string :email
      t.string :telepon
      t.string :alamat

      t.timestamps
    end
  end
end
