class CreateMotors < ActiveRecord::Migration[5.2]
  def change
    create_table :motors do |t|
      t.string :nama
      t.string :email
      t.string :telepon
      t.string :alamat

      t.timestamps
    end
  end
end
