Rails.application.routes.draw do
  resources :motors

  resources :welcome
  
  get 'welcome/index'

  resources :articles do
  	resources :comments

  end

  resources :tables

  root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
